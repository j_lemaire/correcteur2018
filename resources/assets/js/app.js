/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//components pour le spa des Classes
import ClasseIndexComponent from './components/classes/ClassesIndexComponent.vue';
import ClasseEditComponent from './components/classes/ClasseEditComponent.vue';


//modules externes

//permet de faire un spa (single page app) en changeant le component qui est affiché
import VueRouter from 'vue-router';
Vue.use(VueRouter);

//permet d'afficher des alerts
import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);

//permet d'afficher un selecteur (pull down)
import vSelect from 'vue-select'
Vue.component('v-select', vSelect)

// Définition des routes pour VueRouter

const routes = [ //todo: créer des fichiers .js avec les groupes de routes et les importer.
    { path: '/', redirect: '/classeIndex'},
    { path: '/classeIndex', component: ClasseIndexComponent },
    { path: '/classeEdit/:id', component: ClasseEditComponent, props: true },
]

const router = new VueRouter({
    routes // equivalent a 'routes: routes'
})
const app = new Vue({
    router,
    el: '#rootVue'
});
