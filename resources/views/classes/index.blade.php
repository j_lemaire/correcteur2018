@extends('layoutVue')
@section('content')
	<div class="container-fluid">
		<section class="section-padding">
			<div class="jumbotron text-left">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h1> Gestion de classe</h1>

					@if(Auth::user()->ability('','ajout-classe'))
							<a href="{!!action('ClassesController@create') !!}" class="btn btn-primary">Créer une classe</a>
						@endif						
					</div>
					<router-view></router-view>
				</div>
			</div>

		</section>
	</div>

	<!-- TODO: je sais pas pourquoi, mais y faut que js/app.js soit ici, et non dans le header -->

@stop