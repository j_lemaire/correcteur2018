@extends('layout')
@section('content')

<section class="header section-padding">
	<div class="container">
		<div class="header-text">
			<h1>Édition d'une classe</h1>
		</div>
	</div>
</section>

<div class="container">
	<section class="section-padding">
		<div class="jumbotron text-left">
			<classe-edit></classe-edit>
		</div>
	</section>
</div>

<!-- TODO: je sais pas pourquoi, mais y faut que js/app.js soit ici, et non dans le header -->
<script src={{ asset("/js/app.js")}}></script>
@stop