<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SessionResource;
use App\Models\Sessionscholaire;


class ClasseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        SessionResource::withoutWrapping();

        return [
            'data' => [
                'type'  => 'classe',
                'id'    => (string)$this->id,
                'code'  => $this->code,
                'nom'       => $this->nom,
                'groupe'    => $this->groupe,
                'local'     => $this->local,
                'sessionscholaire' => new SessionResource(Sessionscholaire::find($this->sessionscholaire()->first()->id)),

            ],
            'liens' => [
                //TODO: 'show' => route('classes.show', ['classe' => $this->id]),
                'edit' => route('classes.editApi', ['classe' => $this->id]),
                'update' => route('classes.updateApi', ['classe' => $this->id]),
                //'create' => route('classes.create'), //pas nécessaire car y a rien a retourner en json pour un create
                //TODO: 'store' => route('classes.store'),
                'delete' =>  route('classes.deleteApi', ['classe' => $this->id]),
                'tps' => route('tps.index', ['belongsToId' => $this->id]),
                'etudiants' => route('etudiants.index', ['belongsToId' => $this->id]),

            ]

        ];
    }
}
