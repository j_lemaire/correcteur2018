<?php
/**
 * Created by PhpStorm.
 * User: desrosib
 * Date: 2018-09-14
 * Time: 10:22
 */

namespace App\Http\Controllers;

/**
 * Le contorller pour les sessions scholaires
 *
 * @author benou
 *
 */

use App\Models\Sessionscholaire;
use App\Http\Resources\SessionResource;

use Illuminate\Http\Request;


class SessionsController extends Controller
{
    public function listeApi(Request $request) {

        $s = Sessionscholaire::all();
        SessionResource::withoutWrapping(); //
        return SessionResource::collection($s);
    }
}