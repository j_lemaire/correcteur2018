<?php

namespace App\Http\Controllers; 

use App\Http\ControllersGestion\ClassesGestion;
use App\Http\Controllers\BaseResourcesController;
use App\Http\Requests\StoreClasse;
use App\Http\Resources\ClasseResource;

use Illuminate\Http\Request;
use App\Models\Classe;
use App\Models\Sessionscholaire;
use Auth;
use View;

/**
 * Le controller pour les classes
 * 
 * @version 0.1
 * @author benou
 *
 */

class ClassesController extends BaseResourcesController
{
	public function __construct(ClassesGestion $gestion) {
		//parent::__construct();
		$this->gestion = $gestion;
		$this->baseView= "classes"; 
		$this->message_store = 'La classe a été ajoutée';
		$this->message_update = 'La classe a été modifiée';
		$this->message_delete = 'La classe a été effacée';
	}

	public function indexApi(Request $request) {
        //si l'usager connecté peut ajouter des classes, on liste toutes les classes
        //sinon, c'est un professeur, alors on limite la liste des classes aux siennes
        $u = Auth::user();
        if(Auth::user()->ability('','ajout-classe')) {
            $c = Classe::all();
        } else {
            $c = Classe::all()->filter(function($item) use ($u) {return $item->professeurs()->get()->contains("id",$u->id);} );
        }
	    ClasseResource::withoutWrapping(); //
	    return   ClasseResource::collection($c);
        //return    Classe::select('id','nom', 'groupe', 'local' )->get();
    }

    public function editApi( Request $request, $id) {
        ClasseResource::withoutWrapping(); //
        return new ClasseResource(Classe::find($id));
    }

    public function updateApi( StoreClasse $request, $id) {
        // la validation se fait dans StoreClasse qui est un request avec validateur
        // rendu ici, je sais que mon data est bon.

        $classe = Classe::find($id);
        $classe->code = $request['code'];
        $classe->nom = $request['nom'];
        $classe->groupe = $request['groupe'];
        $classe->local = $request['local'];
        $sessionScholaire = Sessionscholaire::find($request['sessionscholaire']['id']); //l'id est bon car c'est validé dans le request

        $sessionScholaire->classes()->save($classe);
    }

	public function deleteApi(Request $request, $id) {
	    Classe::find($id)->delete();
	    return  response()->json();
    }

    /*public function editVue(Request $request, $id)
    {
        $classe = Classe::findOrFail($id);
        return View::make('classes.editVue', compact('classe'));
    }*/
}